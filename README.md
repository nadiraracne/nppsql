NppSql
======
1st November 2022

Documentation
-------------
Documentation can be found by now only in italian on [Blogger](https://shakmati.blogspot.com/2022/11/notepad-e-database-oracle.html).
Hope to produce soon an English version too.

What is NppSql?
---------------
I wanted to find a way to write a query for an Oracle database on Notepad++, launch it, and see the result on Notepad++, in a comfortable way, without too much hussle, too much clicks from the desired goal.
Please, keep in mind that this project is at an early stage of developement.

Requirements
------------
This little project needs:
- **[Notepad++](https://notepad-plus-plus.org/)** installed, with **[NppExec](https://github.com/d0vgan/nppexec)** and **[CsvQuery](https://github.com/jokedst/CsvQuery)** (optional) plugins
- An **[Oracle SqlPlus](https://en.wikipedia.org/wiki/SQL_Plus)** installed

Disclaimer
----------
Use that software at your own risk! This is for personal use only, for my own learning purposes, and maybe it will insipire others to do something better.
It never was intended for production, it hasn't been fully tested, please review, inspect and correct it before any use.
Feel free to send your suggestions and corrections.

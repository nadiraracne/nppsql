--------------------------------------------------
-- Parameters:
--------------------------------------------------
DEFINE _CONN=&1
DEFINE _SCRIPT_NAME=&2
DEFINE _SCRIPT_DIR=&3
DEFINE _SCRIPT_PATH=&&_SCRIPT_DIR\&&_SCRIPT_NAME
DEFINE _CONNECTVERIFYFILE=&&_SCRIPT_DIR\tmp\&&_CONN._connection.log

PROMPT CONN             : &&_CONN
PROMPT SCRIPT_NAME      : &&_SCRIPT_NAME
PROMPT SCRIPT_DIR       : &&_SCRIPT_DIR
PROMPT SCRIPT_PATH      : &&_SCRIPT_PATH
PROMPT CONNECTVERIFYFILE: &&_CONNECTVERIFYFILE


--------------------------------------------------
-- Connection:
--------------------------------------------------
SPOOL &_CONNECTVERIFYFILE;
@@ &&_CONN._connect.sql
SET VERIFY OFF;
PROMPT Connecting To DB With role &&_USER_MAIN at &&_DB_SERVICE_NAME
CONNECT &_CONNECTIONSTRING_MAIN
SPOOL OFF


--------------------------------------------------
-- Filename (without extension):
--------------------------------------------------
COLUMN newfilename new_value _FILENAME noprint
SELECT SUBSTR('&&_SCRIPT_NAME', 1, LENGTH('&&_SCRIPT_NAME')-4) newfilename FROM dual;
PROMPT FILENAME         : &&_FILENAME


--------------------------------------------------
-- Result file:
--------------------------------------------------
DEFINE _RESULTFILE=&&_SCRIPT_DIR\results\&&_FILENAME..csv
SPOOL &_RESULTFILE;


--------------------------------------------------
-- Layout:
--------------------------------------------------
SET ECHO OFF
--SET LINES 500
--SET LONGCHUNKSIZE
--SET TRIMOUT ON
--SET SQLPROMPT ''
SET PAGES 30000
SET HEADING  ON

SET COLSEP ";"
SET HEADSEP OFF
SET PAGESIZE 50000
SET TRIMSPOOL ON

SET LINESIZE 2000
SET NUMWIDTH 10
--SET LONG 99999
SET MARKUP CSV ON DELIMITER ";" QUOTE OFF
--SET UNDERLINE OFF
SET FEEDBACK OFF


--------------------------------------------------
-- Query:
--------------------------------------------------
@@ &&_SCRIPT_PATH


--------------------------------------------------
-- Exit:
--------------------------------------------------
SET ECHO     ON
SET FEEDBACK ON
SET VERIFY   ON
SPOOL OFF;


QUIT;
